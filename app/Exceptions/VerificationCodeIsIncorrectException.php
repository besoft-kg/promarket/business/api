<?php

namespace App\Exceptions;

use Exception;

class VerificationCodeIsIncorrectException extends Exception
{
    public function __construct($attempts = null)
    {
        $this->message = 'Неверный код подтверждения!';
        $this->status = 'code_is_incorrect';
        $this->payload = ['attempts' => $attempts];
        $this->code = 400;
    }
}
