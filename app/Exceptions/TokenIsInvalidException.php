<?php

namespace App\Exceptions;

use Exception;

class TokenIsInvalidException extends Exception
{
    public function __construct($attempts = null)
    {
        $this->message = 'Ваш токен недействителен!';
        $this->status = 'token_is_invalid';
        $this->code = 401;
    }
}
