<?php

namespace App\Exceptions;

use Exception;

class ProductNotFoundException extends Exception
{
    public function __construct()
    {
        $this->message = 'Товар не найден!';
        $this->status = 'product_not_found';
        $this->code = 404;
    }
}
