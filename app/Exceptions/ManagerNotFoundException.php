<?php

namespace App\Exceptions;

use Exception;

class ManagerNotFoundException extends Exception
{
    public function __construct()
    {
        $this->message = 'Управляющий не найден!';
        $this->status = 'manager_not_found';
        $this->code = 404;
    }
}
