<?php

namespace App\Exceptions;

use Exception;

class VerificationCodeNotFoundException extends Exception
{
    public function __construct()
    {
        $this->message = 'Код недействительный или у вас нет попыток!';
        $this->status = 'code_is_invalid';
        $this->code = 404;
    }
}
