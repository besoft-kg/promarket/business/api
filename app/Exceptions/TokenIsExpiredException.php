<?php

namespace App\Exceptions;

use Exception;

class TokenIsExpiredException extends Exception
{
    public function __construct($attempts = null)
    {
        $this->message = 'Срок действия вашего токена истек!';
        $this->status = 'token_is_expired';
        $this->code = 401;
    }
}
