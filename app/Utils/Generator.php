<?php

namespace App\Utils;

use App\Models\Session;
use Carbon\Carbon;
use Firebase\JWT\JWT;

class Generator
{
    static function jwt($user, $sub, $exp)
    {
        $payload = [
            'iss' => "besoft",
            'sub' => ['user' => $user->id, 'key' => $sub],
            'iat' => Carbon::now()->timestamp,
            'exp' => $exp
        ];

        return JWT::encode($payload, env('JWT_SECRET'));
    }

    static function verificationCode()
    {
        //return rand(1000, 9999);
        return 1234;
    }

    static function randomString($length = 32)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    static function sessionKey()
    {
        $key = Generator::randomString(64);

        if (Session::where('key', $key)->exists()) {
            return Generator::sessionKey();
        }

        return $key;
    }
}
