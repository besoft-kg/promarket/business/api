<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Offer extends BaseModel
{
    protected $table = 'offers';

    protected $fillable = [
        'price',
        'description',
        'product_id',
        'discount_percentage',
        'discount_start_on',
        'discount_end_on',
    ];

    protected $appends = [
        'branches',
    ];

    protected $guarded = [
        'id',
        'updated_at',
        'created_at',
    ];

    protected $casts = [
        'price' => 'integer',
    ];

    public function product(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function getBranchesAttribute()
    {
        $manager = Manager::where('user_id', Auth::id())->first();
        if (empty($manager->branch_id)) return [];
        return OfferBranch::where('offer_id', $this->id)->where('branch_id', $manager->branch_id)->get();
    }

    public static function booted()
    {
        static::addGlobalScope(function ($builder) {
            $builder->select(['*', DB::raw(
                'FLOOR(IF(discount_percentage IS NOT NULL AND CURRENT_DATE BETWEEN discount_start_on AND discount_end_on, price - ((price * discount_percentage) / 100), price)) as sale_price,' .
                'FLOOR(IF(discount_percentage IS NOT NULL AND CURRENT_DATE BETWEEN discount_start_on AND discount_end_on, 1, 0)) as discount_active'
            )]);
        });
    }
}
