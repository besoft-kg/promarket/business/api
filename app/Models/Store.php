<?php

namespace App\Models;

class Store extends BaseModel
{
    protected $table = 'stores';

    protected $fillable = [
        'title',
        'contacts',
        'logo_id'
    ];

    protected $guarded = [
        'id',
        'creator_id',
        'updated_at',
        'created_at',
    ];

    protected $casts = [
        'contacts' => 'array',
    ];

    public function logo()
    {
        return $this->hasOne('App\Models\Image', 'id', 'logo_id');
    }
}
