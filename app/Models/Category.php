<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $with = [
        'icon',
    ];

    protected $casts = [
        'all_subcategories' => 'array',
    ];

    protected $hidden = [
        'all_subcategories',
        'created_at',
        'creator_id',
        'icon_id',
        'popularity',
        'updated_at',
    ];

    public function icon() {
        return $this->hasOne(IImage::class, 'id', 'icon_id');
    }
}
