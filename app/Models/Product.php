<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Product extends BaseModel
{
    protected $table = 'products';

    protected $guarded = [
        'id',
        'offers_count',
        'creator_id',
        'updated_at',
        'created_at'
    ];

    protected $appends = [
        'colors',
        'main_image',
    ];

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id', 'id')->with(['groups', 'properties', 'colors']);
    }

    public function brand()
    {
        return $this->belongsTo('App\Models\Brand', 'brand_id', 'id');
    }

    public function getImagesAttribute(): array
    {
        return array_map(function ($v) {
            return [
                'id' => $v['id'],
                'url' => $v['image']['url']['original'],
                'color' => $v['color_id'],
            ];
        }, ProductImage::where('product_id', $this->id)->with('image')->get()->toArray());
    }

    public function getMainImageAttribute()
    {
        return IImage::whereRaw(DB::raw(
            'id IN(SELECT image_id FROM tabekg_product_images WHERE product_id = ? OR product_id = ?)'
        ), [$this->id, $this->parent_id])->first();
    }

    public function getColorsAttribute()
    {
        return Color::whereRaw(DB::raw(
            'id IN(SELECT color_id FROM tabekg_product_colors WHERE product_id = ? OR product_id = ?)'
        ), [$this->id, $this->parent_id])->get();
    }
}
