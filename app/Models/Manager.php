<?php

namespace App\Models;

class Manager extends BaseModel
{
    protected $table = 'managers';

    protected $fillable = [
        'position',
        'store_id',
        'branch_id',
        'user_id',
    ];

    protected $guarded = [
        'id',
        'creator_id',
        'updated_at',
        'created_at',
    ];
}
