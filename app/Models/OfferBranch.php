<?php

namespace App\Models;

class OfferBranch extends BaseModel
{
    protected $table = 'offer_branches';

    protected $fillable = [
        'offer_id',
        'branch_id',
        'color_id',
    ];

    protected $guarded = [
        'id',
        'updated_at',
        'created_at',
    ];
}
