<?php

namespace App\Models;

class Session extends BaseModel
{
    protected $table = 'sessions';

    protected $fillable = [
        'user_agent',
    ];

    protected $guarded = [
        'id',
        'user_id',
        'key',
        'expired_at',
        'updated_at',
        'created_at',
    ];

    protected $hidden = [
        'key',
    ];

    protected static function booted()
    {
        static::creating(function ($item) {
            $item->service = 'business';
        });
    }
}
