<?php

namespace App\Models;

class Sms extends BaseModel
{
    protected $table = 'sms';

    protected $fillable = [
        'external_id',
        'phone',
        'country',
        'region',
        'operator',
        'content',
        'status',
        'response',
        'delivery_report',
        'used_for',
        'payload',
        'service'
    ];

    protected $guarded = [
        'id',
        'parts',
        'amount',
        'price',
        'updated_at',
        'created_at'
    ];

    protected $hidden = [];

    protected $casts = [
        'response' => 'array',
        'payload' => 'array',
        'delivery_report' => 'array',
    ];
}
