<?php

namespace App\Models;

class Branch extends BaseModel
{
    protected $table = 'branches';

    protected $fillable = [
        'title',
        'address',
        'address_lat',
        'address_lng',
        'contacts',
        'schedule',
        'store_id',
    ];

    protected $guarded = [
        'id',
        'creator_id',
        'updated_at',
        'created_at',
    ];

    protected $casts = [
        'contacts' => 'array',
        'schedule' => 'array',
    ];

    protected $with = [
        'region',
    ];

    public function region(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Region::class, 'id', 'region_id');
    }
}
