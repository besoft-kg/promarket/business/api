<?php

namespace App\Models;

class ProductImage extends BaseModel
{
    protected $table = 'product_images';

    protected $fillable = [
        'position',
        'product_id',
        'color_id',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'product_id',
    ];

    public function color()
    {
        return $this->belongsTo('App\Models\Color', 'color_id', 'id');
    }

    public function image()
    {
        return $this->belongsTo('App\Models\Image', 'image_id', 'id');
    }
}
