<?php

namespace App\Models;

class Brand extends BaseModel
{
    protected $table = 'brands';

    protected $guarded = [
        'id',
        'creator_id',
        'updated_at',
        'created_at'
    ];

    public function logo()
    {
        return $this->hasOne('App\Models\Image', 'id', 'logo_id');
    }
}
