<?php

namespace App\Http\Policies;

use App\Models\Manager;
use App\Models\Offer;
use App\Models\User;

class OfferPolicy
{
    public function create(User $user)
    {
        $manager = Manager::where('user_id', $user->id)->first();
        return $manager->store_id !== null && $manager->branch_id === null;
    }

    public function read(User $user)
    {
        return true;
    }

    public function update(User $user, Offer $item)
    {
        $manager = Manager::where('user_id', $user->id)->first();
        return $manager->store_id !== null && $manager->branch_id === null && $item->store_id === $manager->store_id;
    }

    public function delete(User $user)
    {
        return true;
    }
}
