<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\ManagerNotFoundException;
use App\Exceptions\TokenIsExpiredException;
use App\Exceptions\TokenIsInvalidException;
use App\Exceptions\VerificationCodeIsIncorrectException;
use App\Exceptions\VerificationCodeNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Routing\Controller as BaseController;
use Exception;

class Controller extends BaseController
{
    protected $user;
    protected $request;

    public function __construct(Request $request)
    {
        $this->user = Auth::user();
        $this->request = $request;
    }

    public function _dbTransactionAndTryCatch($f)
    {
        $response = null;
        DB::beginTransaction();

        try {
            $response = $f();
            DB::commit();
            return $response;
        } catch (Exception $e) {
            if (
                $e instanceof ManagerNotFoundException ||
                $e instanceof TokenIsExpiredException ||
                $e instanceof VerificationCodeIsIncorrectException ||
                $e instanceof VerificationCodeNotFoundException ||
                $e instanceof TokenIsInvalidException
            ) DB::commit();
            else DB::rollback();
            throw $e;
        }
    }

    public function _checkWith($name): bool
    {
        return in_array($name, explode(',', $this->request->get('_with')));
    }

    public function _getOptionalParams($args, $additional = null): array
    {
        switch ($additional) {
            case 'get':
                if (empty($args['id'])) $args['id'] = null;
                if (empty($args['_limit']) || $args['_limit'] === 0) $args['_limit'] = 30;
                if (empty($args['_sorting'])) $args['_sorting'] = 'desc';
                if (empty($args['_order_by'])) $args['_order_by'] = 'id';
                if (empty($args['_after_by'])) $args['_after_by'] = null;
                if (empty($args['_after'])) $args['_after'] = null;
                if (empty($args['_with'])) $args['_with'] = null;
                break;
            default:
                break;
        }

        $result = [];
        foreach ($args as $key => $value) {
            $default = null;
            if (!is_integer($key)) {
                $default = $value;
                $value = $key;
            }
            $request_value = $this->request->get($value);

            if ($this->request->has($value)) {
                $result[$value] = $request_value;
            } else {
                $result[$value] = $default;
            }
        }
        return $result;
    }

    public function _validate($rules, $for = null, $payload = null): array
    {
        $validators = [];

        switch ($for) {
            case 'get':
                $validators = [
                    'id' => 'bail|integer',
                    '_order_by' => 'bail|string|in:id,created_at,updated_at' . ($payload ? ',' . $payload : ''),
                    '_sorting' => 'bail|string|in:desc,asc',
                    '_limit' => 'bail|integer|min:-1|max:30',
                    '_after_by' => 'bail|string|in:id,created_at,updated_at' . ($payload ? ',' . $payload : ''),
                    '_after' => 'bail',
                    '_with' => 'bail|string',
                ];
                break;
            default:
                break;
        }

        $this->validate($this->request, array_merge(
            $validators,
            $rules
        ));

        return $this->_getOptionalParams(array_keys($rules), $for);
    }
}
