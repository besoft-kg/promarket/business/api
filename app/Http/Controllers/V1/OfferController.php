<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\ProductNotFoundException;
use App\Models\Manager;
use App\Models\Offer;
use App\Models\OfferBranch;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;

class OfferController extends Controller
{
    public function post_index()
    {
        /*
            * Response:
            *   -exists
          */

        $this->authorize('create', Offer::class);

        $params = $this->_validate([
            'items' => 'bail|array',
            'items.*.product_id' => 'bail|required|integer',
            'items.*.price' => 'bail|required|integer',
            'items.*.used' => 'bail|required|boolean',

            'description' => 'bail|string|max:1000',
            'product_id' => 'bail|integer',
            'price' => 'bail|integer',
            'used' => 'bail|boolean',
            'discount_percentage' => 'bail|integer|min:0|max:99',
            'start_discount' => 'bail|string',
            'end_discount' => 'bail|string',
        ]);

        $manager = Manager::where('user_id', Auth::id())->first();

        if (isset($params['items'])) {

            foreach ($params['items'] as $g) {
                $product = Product::find($g['product_id']);
                if (!$product) throw new ProductNotFoundException();

                $item = Offer::firstOrNew([
                    'store_id' => $manager->store_id,
                    'product_id' => $g['product_id'],
                ]);
                $item->price = $g['price'];
                $item->store_id = $manager->store_id;
                $item->used = $g['used'];

                if (!$item->save()) throw new \Exception();
            }

        } else {

            $product = Product::find($params['product_id']);
            if (!$product) throw new ProductNotFoundException();

            $item = Offer::firstOrNew([
                'store_id' => $manager->store_id,
                'product_id' => $params['product_id'],
            ]);

            $item->description = isset($params['description']) && strlen($params['description']) > 0 ? $params['description'] : null;
            $item->price = $params['price'];
            $item->store_id = $manager->store_id;
            $item->used = $params['used'];
            if (isset($params['discount_percentage']) && $params['discount_percentage'] > 0) {
                $item->discount_percentage = $params['discount_percentage'];
                $item->discount_start_on = $params['start_discount'];
                $item->discount_end_on = $params['end_discount'];
            } else {
                $item->discount_percentage = null;
                $item->discount_start_on = null;
                $item->discount_end_on = null;
            }

            if (!$item->save()) throw new \Exception();

            return $item;

        }

        return [];
    }

    public function post_branch()
    {
        $params = $this->_validate([
            'offer_id' => 'bail|required|integer',
            'colors' => 'bail|nullable|array',
        ]);

        $manager = Manager::where('user_id', Auth::id())->first();

        if (isset($params['colors'])) {

            $active_offers = [];

            foreach ($params['colors'] as $color) {
                $item = new OfferBranch([
                    'branch_id' => $manager->branch_id,
                    'offer_id' => $params['offer_id'],
                    'color_id' => $color['color_id'],
                ]);
                $item->save();
                array_push($active_offers, $item->id);
            }

            OfferBranch::whereNotIn('id', $active_offers)->where('offer_id', $params['offer_id'])->where('branch_id', $manager->branch_id)->delete();

            return OfferBranch::whereIn('id', $active_offers)->where('branch_id', $manager->branch_id)->where('offer_id', $params['offer_id'])->get();

        } else {

            if (OfferBranch::where('branch_id', $manager->branch_id)->where('offer_id', $params['offer_id'])->count() > 0) {
                OfferBranch::where('branch_id', $manager->branch_id)->where('offer_id', $params['offer_id'])->delete();
                return null;
            } else {
                $item = new OfferBranch([
                    'branch_id' => $manager->branch_id,
                    'offer_id' => $params['offer_id'],
                ]);
                $item->save();
                return $item;
            }

        }
    }

    public function get_index()
    {
        $params = $this->_validate([
            'id' => 'bail|integer|required',
            //'store_id' => 'bail|required|integer',
        ], 'get');

        $this->authorize('read', Offer::class);

        $manager = Manager::where('user_id', Auth::id())->first();

        return Offer::where('id', $params['id'])->with('product')->where('store_id', $manager->store_id)->firstOrFail();
    }

    public function delete_index()
    {
        $params = $this->_validate([
            'id' => 'bail|integer|required',
        ], 'get');

        $this->authorize('delete', Offer::class);

        $manager = Manager::where('user_id', Auth::id())->first();

        OfferBranch::where('offer_id', $params['id'])->delete();
        Offer::where('id', $params['id'])->where('store_id', $manager->store_id)->delete();

        return [];
    }
}
