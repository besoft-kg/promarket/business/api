<?php

namespace App\Http\Controllers\V1;

use App\Models\Branch;
use App\Models\Manager;
use App\Models\Offer;
use Illuminate\Support\Facades\Auth;

class ToController extends Controller
{
    public function get_home()
    {
        $manager = Manager::where('user_id', Auth::id())->first();

        if ($manager->branch_id) {
            return Offer::with(['product'])->where('store_id', $manager->store_id)->get();
        } else {
            return [
                'offers' => Offer::with('product')->where('store_id', $manager->store_id)->get(),
                'branches' => Branch::with('region')->where('store_id', $manager->store_id)->get(),
            ];
        }
    }
}
