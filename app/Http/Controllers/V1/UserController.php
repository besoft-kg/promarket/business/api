<?php

namespace App\Http\Controllers\V1;

use App\Models\Branch;
use App\Models\Category;
use App\Models\Manager;
use App\Models\Store;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function getBasic(): array
    {
        $user = Auth::user();
        $manager = Manager::where('user_id', $user->id)->first();

        return [
            'user' => $user,
            'manager' => $manager,
            'store' => Store::find($manager->store_id),
            'branch' => Branch::find($manager->branch_id),
            'categories' => Category::all(),
        ];
    }
}
