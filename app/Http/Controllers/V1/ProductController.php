<?php

namespace App\Http\Controllers\V1;

use App\Models\Manager;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function get_search()
    {
        $params = $this->_validate([
            'query' => 'bail|required|string|min:3',
        ]);

        $manager = Manager::where('user_id', Auth::id())->first();

        return Product::whereRaw(DB::raw('title LIKE ? AND (parent_id IS NOT NULL OR NOT EXISTS(SELECT 1 FROM tabekg_products as p WHERE p.parent_id = tabekg_products.id)) AND NOT EXISTS(SELECT 1 FROM tabekg_offers WHERE store_id = ? AND product_id = tabekg_products.id)'), ['%' . $params['query'] . '%', $manager->store_id])->get();
    }
}
