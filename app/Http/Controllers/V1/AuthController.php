<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\ManagerNotFoundException;
use App\Exceptions\VerificationCodeIsIncorrectException;
use App\Exceptions\VerificationCodeNotFoundException;
use App\Models\Branch;
use App\Models\Category;
use App\Models\ConfirmPhone;
use App\Models\Manager;
use App\Models\Session;
use App\Models\Store;
use App\Models\User;
use App\Models\Sms;
use App\Utils\Generator;
use Carbon\Carbon;

class AuthController extends Controller
{
    public function postPhone()
    {
        /*
         * Response:
         *      -user_not_found
         *      code_is_sent
         *      code_was_sent
         */

        $params = $this->_validate([
            'phone_number' => 'bail|required|regex:/^[9][9][6][0-9]{9}$/'
        ]);

        $phone_number = $params['phone_number'];

        $user = User::where('phone_number', $phone_number)->first();

        if (!$user || Manager::where('user_id', $user->id)->count() <= 0) throw new ManagerNotFoundException();

        $confirm_phone = ConfirmPhone::where('user_id', $user->id)->where('confirmed', false)->where('attempts', '>', 0)->first();

        if ($confirm_phone) {
            return ['status' => 'code_was_sent'];
        }

        $code = Generator::verificationCode();

        return $this->_dbTransactionAndTryCatch(function () use ($user, $code) {

            $sms = new Sms();
            $sms->external_id = 'Besoft_SMS_' . Carbon::now()->timestamp;
            $sms->phone = $user->phone_number;
            $sms->content = 'Код подтверждения вашего номера: ' . $code;
            $sms->parts = 1;
            $sms->amount = 1.19;
            $sms->price = 1.19;
            $sms->status = 'sent_to_service';
            $sms->used_for = 'business-sign-in';
            $sms->payload = ['code' => $code];
            $sms->service = 'nikita';

            $sms->save();

            $confirm_phone = new ConfirmPhone();
            $confirm_phone->sms_id = $sms->id;
            $confirm_phone->user_id = $user->id;
            $confirm_phone->phone = $user->phone_number;
            $confirm_phone->encrypted_code = md5($code . env('APP_KEY'));

            $confirm_phone->save();

            return ['status' => 'code_is_sent'];
        });
    }

    public function postPhoneCheck()
    {
        /*
         * Response:
         *      -code_is_incorrect
         *      -code_is_invalid
         *      -user_not_found
         */

        $params = $this->_validate([
            'phone_number' => 'bail|required|integer|regex:/^[9][9][6][0-9]{9}$/',
            'verification_code' => 'bail|required|integer|digits:4',
            'version_code' => 'bail|required|integer',
        ]);

        $user = User::where('phone_number', $params['phone_number'])->first();

        if (!$user || Manager::where('user_id', $user->id)->count() <= 0) throw new ManagerNotFoundException();

        $confirm_phone = ConfirmPhone::where('user_id', $user->id)->where('confirmed', false)->where('attempts', '>', 0)->orderBy('created_at', 'desc')->first();

        if (!$confirm_phone) throw new VerificationCodeNotFoundException();

        return $this->_dbTransactionAndTryCatch(function () use ($params, $confirm_phone, $user) {
            if ($confirm_phone->encrypted_code === md5($params['verification_code'] . env('APP_KEY'))) {
                $confirm_phone->confirmed = true;
                $confirm_phone->save();

                $expired_at = Carbon::now()->addMonth();
                $session_key = Generator::sessionKey();

                $session = new Session();
                $session->user_id = $user->id;
                $session->key = $session_key;
                $session->expired_at = $expired_at;
                $session->user_agent = $this->request->header('User-Agent');
                $session->version_code = $params['version_code'];
                $session->save();

                $manager = Manager::where('user_id', $user->id)->first();

                return [
                    'token' => Generator::jwt($user, $session_key, $expired_at->timestamp),
                    'user' => $user,
                    'categories' => Category::all(),
                    'manager' => $manager,
                    'store' => $manager->store_id ? Store::find($manager->store_id) : null,
                    'branch' => $manager->branch_id ? Branch::find($manager->branch_id) : null,
                ];
            } else {
                $confirm_phone->attempts--;
                $confirm_phone->save();
                throw new VerificationCodeIsIncorrectException($confirm_phone->attempts);
            }
        });
    }
}
