<?php

namespace App\Providers;

use App\Exceptions\TokenIsExpiredException;
use App\Exceptions\TokenIsInvalidException;
use App\Models\Offer;
use App\Models\Session;
use App\Models\User;
use App\Http\Policies\OfferPolicy;
use Exception;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        Gate::policy(Offer::class, OfferPolicy::class);

        $this->app['auth']->viaRequest('api', function ($request) {
            $header = $request->header('Authorization', '');
            $token = null;

            if (Str::startsWith($header, 'Bearer ')) {
                $token = Str::substr($header, 7);
            }

            if ($token) {
                try {
                    $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
                } catch (ExpiredException $e) {
                    throw new TokenIsExpiredException();
                } catch (Exception $e) {
                    throw new TokenIsInvalidException();
                }

                $session = Session::where('key', $credentials->sub->key)->where('service', 'business')->where('user_id', $credentials->sub->user)->first();

                if (!$session) throw new TokenIsInvalidException();

                return User::where('id', $session->user_id)->first();
            } else {
                return null;
            }
        });
    }
}
