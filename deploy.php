<?php

namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'ProMarket Business');
set('default_stage', 'production');

// Project repository
set('repository', 'git@gitlab.com:besoft-kg/promarket/business/api.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
add('shared_files', [
    '.env'
]);
add('shared_dirs', [
    'storage',
    'public/uploads'
]);

// Writable dirs by web server
add('writable_dirs', [
    'bootstrap/cache',
    'storage',
    'storage/app',
    'storage/app/public',
    'storage/framework',
    'storage/framework/cache',
    'storage/framework/sessions',
    'storage/logs',
    'public/uploads',
]);

// Hosts

host('157.230.25.13')
    ->user('deployer')
    ->identityFile('~/.ssh/deployer_rsa')
    ->stage('production')
    ->set('deploy_path', '/var/www/html/promarket/business/api');

// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

task('deploy', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
    'artisan:cache:clear',
    'deploy:symlink',
    //'artisan:db:seed',
    'deploy:unlock',
    'cleanup',
]);

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');

