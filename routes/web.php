<?php

$router->group([
    'namespace' => 'V1',
    'prefix' => 'v1'
], function () use ($router) {
    $router->group(['middleware' => ['auth']], function () use ($router) {

        // AUTH REQUIRED

        $router->get('/user/basic', 'UserController@getBasic');

        $router->get('/to/home', 'ToController@get_home');

        $router->get('/region', 'RegionController@get');

        $router->get('/product/search', 'ProductController@get_search');

        $router->post('/store', 'StoreController@postIndex');
        $router->get('/store', 'StoreController@getIndex');

        $router->post('/offer', 'OfferController@post_index');
        $router->get('/offer', 'OfferController@get_index');
        $router->delete('/offer', 'OfferController@delete_index');

        $router->post('/offer/branch', 'OfferController@post_branch');

        $router->post('/branch', 'BranchController@postIndex');
        $router->get('/branch', 'BranchController@getIndex');
    });

    $router->group([], function () use ($router) {

        // AUTH

        $router->post('/auth/phone', 'AuthController@postPhone');
        $router->post('/auth/phone/check', 'AuthController@postPhoneCheck');
    });
});

$router->get('/', function () {
    return response(['message' => 'ProMarket Business API is running...']);
});
